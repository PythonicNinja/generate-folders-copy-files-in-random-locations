# -*- coding: utf-8 -*-
# CREATED ON DATE: 16.03.15
__author__ = 'vojtek.nowak@gmail.com'


import os
import random
import shutil

from optparse import OptionParser


class FolderGenerator(object):

    def __init__(self):
        self.folders_generated_paths = []
        self.cur_dir = os.path.dirname(os.path.abspath(__file__))

        self.parse_options()
        self.generate_random_file_location()

    def parse_options(self):
        parser = OptionParser()
        parser.add_option("-d", "--depth", dest="depth",
                          help="depth of folders", metavar="FILE")
        parser.add_option("-c", "--count", dest="count",
                          help="count of folders on each level", metavar="FILE")
        parser.add_option("-f", "--files", dest="files",
                          help="files to put into folders", metavar="FILE")
        self.options, self.args = parser.parse_args()

        self.options.depth = int(self.options.depth)
        self.options.count = int(self.options.count)
        self.files_to_put = []
        if self.options.files:
            self.files_to_put = os.listdir(os.path.join(self.cur_dir, str(self.options.files)))

        self.file_path_map = {}

    def generate_random_file_location(self):
        for d in range(self.options.depth):
            path = []
            path.append(d)
            for i, c in enumerate(range(self.options.count)):
                path.append(c)
                self.folders_generated_paths.append(os.path.join(self.cur_dir, "\\".join(map(str, path)), str(i)))

        for file in self.files_to_put:
            self.file_path_map[str(file)] = random.choice(self.folders_generated_paths)

    def create_folders(self):
        print self.file_path_map
        print self.folders_generated_paths
        for path in self.folders_generated_paths:
            if not os.path.exists(path):
                os.makedirs(path)

    def copy_files_to_folder(self):
        for file, path in self.file_path_map.iteritems():
            try:
                shutil.copy(os.path.join(self.cur_dir, str(self.options.files), str(file)), path)
            except:
                pass

if __name__ == "__main__":
    generator = FolderGenerator()
    generator.create_folders()
    generator.copy_files_to_folder()